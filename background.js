chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
  if (request.action === "executeScript") {
    chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
      chrome.tabs.executeScript(tabs[0].id, {code: 'document.documentElement.outerHTML'}, function(results) {
        if (chrome.runtime.lastError || !results || results.length === 0) {
          sendResponse({result: null});
        } else {
          sendResponse({result: results[0]});
        }
      });
    });

    return true;
  }
});
