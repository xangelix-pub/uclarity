import data from './datastore.json' assert { type: 'json' };

chrome.runtime.sendMessage({action: "executeScript"}, function(response) {
  for (let key in data.jsonData) {
    var d = response.result.search(key);
    if (d >= 0) {
      var div = document.getElementById('iheader');
      if (div) {
        div.textContent += key || 'Cannot access page';
      }
      var div = document.getElementById('content');
      if (div) {
        div.textContent += data.jsonData[key] || 'Cannot access page';
      }
    }
  }
});
